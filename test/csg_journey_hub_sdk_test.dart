import 'package:csg_journey_hub_sdk/csg_journey_hub_sdk_method_channel.dart';
import 'package:csg_journey_hub_sdk/csg_journey_hub_sdk_platform_interface.dart';
import 'package:csg_journey_hub_sdk/ip_address/ip_address.dart';
import 'package:csg_journey_hub_sdk/models/battery_level.dart';
import 'package:csg_journey_hub_sdk/models/device_info.dart';
import 'package:csg_journey_hub_sdk/models/device_properties.dart';
import 'package:csg_journey_hub_sdk/models/event_data.dart';
import 'package:csg_journey_hub_sdk/models/log_data.dart';
import 'package:csg_journey_hub_sdk/models/params.dart';
import 'package:csg_journey_hub_sdk/models/user_location.dart';
import 'package:csg_journey_hub_sdk/network/configuration.dart';
import 'package:csg_journey_hub_sdk/network/network_client.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockCsgJourneyHubSdkPlatform
    with MockPlatformInterfaceMixin
    implements CsgJourneyHubSdkPlatform {}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  final CsgJourneyHubSdkPlatform initialPlatform =
      CsgJourneyHubSdkPlatform.instance;

  test('$MethodChannelCsgJourneyHubSdk is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelCsgJourneyHubSdk>());
  });

  /// Test Device IP Address
  test("Test IP Address", () async {
    expect("103.98.79.58", isNot(await IPAddress.getIPAddress()));
  });

  /// Test Sync Events to server
  /// Server returns same event information
  test("Sync Events to Server", () async {
    // Test Data
    var deviceProperties = const DeviceProperties(
        name: "j7duolte",
        model: "SM-J720F",
        systemName: "Android",
        osVersion: "26",
        localizedModel: "j7duolte",
        identifierForVendor: "R16NW");

    var deviceInfo = const DeviceInfo(
        ipAddress: "103.98.79.58",
        operatingSystem: "android",
        channel: "mobile",
        systemVersion: "8.0.0",
        carrier: "CellOne",
        network: "wifi",
        language: "en_IN",
        totalStorage: "23.47 GB",
        totalMemory: "3.60 GB",
        sdkVersion: "0.0.1",
        appVersion: "0.0.1",
        manufacturer: "samsung",
        displayHeight: 720,
        displayWidth: 1280,
        installId: "5f7f02b9-2e5c-4ddb-b2c4-2b101e3e84dd");

    var userLocation = const UserLocation(lat: "0.0", long: "0.0");

    var batteryLevel = const BatteryLevel(
        initialSessionBatteryLevel: 20.0,
        initialSessionBatteryTime: 1667457611433,
        eventBatteryLevel: 24.0);

    Params params = const Params(
        globalParams: {"appName": "CSGJourneyHubSDK Test"},
        requestParams: {"eventId": "23144", "event": "Login Event"});

    var eventInfo = EventData(
        automatic: false,
        name: "Login Button Click Event",
        timestamp: 1667457611433,
        params: params);

    var logData = LogData(
        deviceInfo: deviceInfo.toMap(),
        deviceProperties: deviceProperties.toMap(),
        location: userLocation.toMap(),
        batteryLevel: batteryLevel.toMap(),
        eventInfo: eventInfo.toMap());

    var configuration =
        const Configuration(endpoint: "API_END_POINT", key: "API_KEY");

    final url = configuration.endpoint + configuration.key;
    await NetworkClient.instance
        .recordEvent(url, logData.toMap())
        .then((value) async {
      expect(isNotEmpty, isNot(value.toString()));
    }).onError((error, stackTrace) {
      expect(isNotEmpty, error.toString());
    });
  });
}
