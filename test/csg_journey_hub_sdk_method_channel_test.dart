import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';

void main() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  const MethodChannel channel = MethodChannel('csg_journey_hub_sdk');
  MethodCall? methodCall;
  dynamic returnValue;

  setUp(() async {
    methodCall = null;
    returnValue = null;

    TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger
        .setMockMethodCallHandler(
      channel,
      (MethodCall call) async {
        methodCall = call;
        return returnValue;
      },
    );
  });

  /// Test battery Level
  test('Get Device Battery Level', () async {
    var batterLevel = await channel.invokeMethod("getBatteryLevel");
    expect(methodCall, isMethodCall('getBatteryLevel', arguments: null));
    if (Platform.isIOS || Platform.isAndroid) {
      expect(isNotEmpty, isNot(batterLevel));
      expect(greaterThan(0), batterLevel);
    }
  });

  /// Test Device Location
  test('Get Device Location', () async {
    var location = await channel.invokeMethod("getLocation");
    expect(methodCall, isMethodCall('getLocation', arguments: null));
    if (Platform.isIOS || Platform.isAndroid) {
      expect(isNotEmpty, isNot(location));
    }
  });

  /// Test Device Details
  test('Get Device Details', () async {
    var deviceDetails = await channel.invokeMethod("getDeviceDetails");
    expect(methodCall, isMethodCall('getDeviceDetails', arguments: null));
    if (Platform.isIOS || Platform.isAndroid) {
      expect(isNotEmpty, isNot(deviceDetails));
      if (deviceDetails != null) {
        expect(isNotEmpty, isNot(deviceDetails["carrier"]));
        expect(isNotEmpty, isNot(deviceDetails["totalStorage"]));
        expect(isNotEmpty, isNot(deviceDetails["totalMemory"]));
        expect(isNotEmpty, isNot(deviceDetails["version"]));
        expect(isNotEmpty, isNot(deviceDetails["screenWidth"]));
        expect(isNotEmpty, isNot(deviceDetails["screenWidth"]));
      }
    }
  });
}
