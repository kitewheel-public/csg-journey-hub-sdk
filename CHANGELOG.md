## 0.0.3

* Fixed bugs
* Updated dependent packages.

## 0.0.2

* Fixed lint warnings
* Bump `plugin_platform_interface` dependency
* Updated package description.

## 0.0.1

* Initial release.
