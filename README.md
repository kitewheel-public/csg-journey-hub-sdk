# CSG-Journey-Hub-SDK Overview

- The CSG-Journey Hub SDK will make it easy to track the user journey throughout the mobile application by enabling customers to add a plugin to their mobile application from which they can gather information from their mobile users.
- This information can be used for orchestration and analytics purposes.
- Plugin will be flutter based for android and IOs applications.
- Plugin has a mechanism to tag individual events, to give the mobile app developer the ability to determine on an event-by-event basis which of them need to be excluded from tracking.
- Also providing a mechanism where a mobile developer can track the screen views.
  In the end, this plugin will share the event payload with the described Endpoint.

### Installation

Add the CSGJourneyHubSDk as a dependency to your Flutter application:

```bash
flutter pub add csg_journey_hub_sdk
```

This will add a line with the dependency like this to your pubspec.yaml:

```yml
dependencies:
  csg_journey_hub_sdk: <latest-version>
```

### Features in Journey Hub SDK:

Before you can instrument, you must initialize the SDK using the API key. For example

```dart

CSGJourneyHubSDK.initializeApp(
        endpoint: "API_END_POINT", key: "API_KEY", globalParams: {"key": "Value"});
```

### Set event tracking by passing value true

```dart
CSGJourneyHubSDK.setEventTrackingEnabled(true);
```

### Stop event tracking by passing value false

```dart
CSGJourneyHubSDK.setEventTrackingEnabled(false);
```

Note: By default tracking will be on, unless user set’s it to false

### The Journey Hub SDK event tracking:

This includes tracking events on the mobile application. Additionally, this event tracker also supports the sending of a payload of contextual data.
Developers will need to record them by calling a method from the SDK with specific event name and attributes if any.

```dart
CSGJourneyHubSDK.logEvent(
        name: "Login Button Clicked",
        requestParams: {"login_event": "Login event.", "event_id": "12151515"});
```

### Global parameters:

Global parameter is an object with every request, where that object is set once per install. The object should be defined globally and be included with every request (this is in addition to the params object that already exists)

```dart
CSGJourneyHubSDK.initializeApp(
        endpoint: API_END_POINT, key: API_KEY, globalParams: {"key": "Value"});
```

### Automatic screen tracking:

For automatic tracking of screen views the existing flutter application will be using default navigation provided by Flutter SDK.
CSGJourneyHubSDK will automatically track your views when supplied as navigatorObservers in your material app. Internally it checks for the name of the route being navigated too on the navigationSetting parameter and logs that as your screen name. Open your main.dart file and add the observer as navigatorObservers.

```dart
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "CSGJourneyHubSDK",
      navigatorObservers: [
        CSGJourneyHubSDK.getCSGRouteObserver()
      ],
    );
  }
}
```

Your view will now be tracked, If, you use named routing, if you don’t then you have to make a small adjustment everywhere you navigate directly to a route. You have to supply the name of the route where you navigate like below.

```dart
Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => LoginView(),
            settings: RouteSettings(name: LoginView)));
```

You have to do that for every navigation you do if you’re not using named routing, Once you’ve done that you’ll get all your page views.

## Copyright and License 
Copyright 2023, CSG International, Inc

Licensed under the [BSD-2](https://opensource.org/license/bsd-2-clause/) license. See the LICENSE file for more info.
