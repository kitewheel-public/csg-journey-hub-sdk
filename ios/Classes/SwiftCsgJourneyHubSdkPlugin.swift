import Flutter
import UIKit
import CoreLocation

public class SwiftCsgJourneyHubSdkPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "csg_journey_hub_sdk", binaryMessenger: registrar.messenger())
        let instance = SwiftCsgJourneyHubSdkPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch(call.method) {
        case "getLocation":
            getLocation(result: result);
            break;
        case "getDeviceDetails":
            getDeviceInformation(result: result);
            break;
        case "getBatteryLevel":
            getBatteryLevel(result: result)
            break;
        default:
            break;
        }
    }
    
    /// Get device location
    private func getLocation(result: @escaping FlutterResult) {
        let manager = CLLocationManager()
        
        let authorizationStatus: CLAuthorizationStatus
        
        if #available(iOS 14, *) {
            authorizationStatus = manager.authorizationStatus
        } else {
            authorizationStatus = CLLocationManager.authorizationStatus()
        }
        switch authorizationStatus {
        case .notDetermined,.denied,.restricted :
            result(nil)
            break;
        case .authorized,.authorizedAlways,.authorizedWhenInUse:
            if manager.location != nil {
                var data = [String: Any]()
                data["lat"] = manager.location?.coordinate.latitude
                data["long"] = manager.location?.coordinate.longitude
                result(data)
            } else {
                result(nil)
            }
            break;
        default:
            result(nil)
            break;
            
        }
    }
    
    /// Get device information
    private func getDeviceInformation(result: @escaping FlutterResult) {
        var deviceInfoMap = [String: Any]()
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        var height = bounds.size.height
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        deviceInfoMap["totalStorage"] = String(format: "%.2f", Double(totalDiskSpaceInBytes / (1024 * 1024 * 1024)))
        deviceInfoMap["totalMemory"] =  String(format: "%.2f",Double(ProcessInfo.processInfo.physicalMemory)/(1024.0 * 1024.0 * 1024.0))
        deviceInfoMap["screenHeight"] = height
        deviceInfoMap["screenWidth"] = width
        deviceInfoMap["version"] = appVersion
        deviceInfoMap["carrier"] = "Unknown"
        result(deviceInfoMap)
    }
    
    /// Get Device battery level
    private func getBatteryLevel(result: @escaping FlutterResult) {
        UIDevice.current.isBatteryMonitoringEnabled = true
        
        var batteryLevel = Double(UIDevice.current.batteryLevel) * 100
        result(Double(String(format: "%.2f",batteryLevel)))
    }
    
    /// Get device total disk space
    private var totalDiskSpaceInBytes:Int64 {
        guard let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
              let space = (systemAttributes[FileAttributeKey.systemSize] as? NSNumber)?.int64Value else { return 0 }
        return space
    }
    
}
