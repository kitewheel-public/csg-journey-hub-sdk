#import "CsgJourneyHubSdkPlugin.h"
#if __has_include(<csg_journey_hub_sdk/csg_journey_hub_sdk-Swift.h>)
#import <csg_journey_hub_sdk/csg_journey_hub_sdk-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "csg_journey_hub_sdk-Swift.h"
#import <CoreLocation/CoreLocation.h>
#endif

@implementation CsgJourneyHubSdkPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftCsgJourneyHubSdkPlugin registerWithRegistrar:registrar];
}
@end
