package com.example.csg_journey_hub_sdk

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import android.app.ActivityManager
import android.content.res.Resources
import android.os.Bundle
import android.os.Environment
import android.os.StatFs
import android.os.BatteryManager
import android.content.Intent
import android.content.IntentFilter
import android.telephony.TelephonyManager
import java.io.File

/** CsgJourneyHubSdkPlugin */
class CsgJourneyHubSdkPlugin : FlutterPlugin, MethodCallHandler {
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private lateinit var channel: MethodChannel
    private lateinit var context: Context;
    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "csg_journey_hub_sdk")
        channel.setMethodCallHandler(this)
        context = flutterPluginBinding.applicationContext
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            "getPlatformVersion" -> result.success("Android ${android.os.Build.VERSION.RELEASE}")
            "getBatteryLevel" -> result.success(getBatteryPercentage(context))
            "getDeviceDetails" -> getDeviceInformation(result)
            "getLocation" -> getLocation(result)
        }
    }

    /// Get Device information totalStorage,totalMemory,screenHeight,screenWidth,carrier,version
    private fun getDeviceInformation(@NonNull result: Result) {
        val totalStorage =
            (getTotalStorage().toDouble() / (1024.0 * 1024.0 * 1024.0)).toDouble()

        val totalMemory = (getTotalMemory().toDouble() / (1024.0 * 1024.0 * 1024.0)).toDouble();
        val screenHeight = getScreenWidth().toDouble();
        val screenWidth = getScreenHeight().toDouble();
        val operatorName =
            (context.getSystemService(Context.TELEPHONY_SERVICE) as? TelephonyManager)?.networkOperatorName
                ?: "Unknown"

        val packageManager = context!!.packageManager
        val info = packageManager.getPackageInfo(context!!.packageName, 0)

        val deviceInfoMap: MutableMap<String, Any> = HashMap()
        deviceInfoMap["totalStorage"] = String.format("%.2f", totalStorage)
        deviceInfoMap["totalMemory"] = String.format("%.2f", totalMemory)
        deviceInfoMap["screenHeight"] = screenHeight
        deviceInfoMap["screenWidth"] = screenWidth
        deviceInfoMap["carrier"] = operatorName
        deviceInfoMap["version"] = info.versionName
        result.success(deviceInfoMap)
    }

    /// Get Device total storage
    private fun getTotalStorage(): Long {
        val iPath: File = Environment.getDataDirectory()
        val iStat = StatFs(iPath.path)
        val iBlockSize = iStat.blockSizeLong
        val iTotalBlocks = iStat.blockCountLong
        return (iTotalBlocks * iBlockSize)
    }

    /// Get device total memory
    private fun getTotalMemory(): Long {
        val actManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val memInfo = ActivityManager.MemoryInfo()
        actManager.getMemoryInfo(memInfo)
        return memInfo.totalMem
    }

    /// Get device screen height
    fun getScreenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }

    /// Get device screen width
    fun getScreenHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    /// Get device location
    private fun getLocation(result: Result) {
        if (ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            ) == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val fusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(context)
            fusedLocationProviderClient.lastLocation.addOnSuccessListener {
                val locationMap: MutableMap<String, Any> = HashMap()
                if (it != null) {
                    locationMap["lat"] = it.latitude
                    locationMap["long"] = it.longitude
                    result.success(locationMap)
                } else {
                    result.success(null)
                }
            }
        } else {
            result.success(null)
        }
    }

    /// Get Device battery percentage
    fun getBatteryPercentage(context: Context): Double {
        val iFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        val batteryStatus = context.registerReceiver(null, iFilter)
        val level = batteryStatus?.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) ?: -1
        val scale = batteryStatus?.getIntExtra(BatteryManager.EXTRA_SCALE, -1) ?: -1
        val batteryPct = level / scale.toFloat()
        return (batteryPct * 100).toDouble()
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }
}
