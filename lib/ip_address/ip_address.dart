import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

enum Format { json, text }

@immutable
class IPAddress {
  const IPAddress();

  /// Scheme https
  static const String _scheme = 'https';

  /// Host name
  static const String _hostV4 = 'api.ipify.org';
  static const String _path = '/';

  /// Response format
  static const String _format = 'format=';

  /// The getIPAddress method is used to retrieve ip address
  static Future<String> getIPAddress({Format format = Format.text}) async {
    final uri =
        Uri(host: _hostV4, path: _path, scheme: _scheme, query: _param(format));
    return await _send(uri);
  }

  /// Calling api to get ip address
  static Future<String> _send(Uri uri) async {
    try {
      final response = await http.get(uri);

      if (response.statusCode != 200) {
        throw Exception(
            'Received an invalid status code from ipify: ${response.statusCode}. The service might be experiencing issues.');
      }

      return response.body;
    } catch (e) {
      throw Exception(
          "The request failed because it wasn't able to reach the ipify service. This is most likely due to a networking error of some sort.");
    }
  }

  /// Parameter format
  static String _param(Format format) =>
      _format + (Format.text == format ? '' : 'json');
}
