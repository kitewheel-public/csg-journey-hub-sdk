import 'package:csg_journey_hub_sdk/constants/csg_event_constants.dart';
import 'package:flutter/cupertino.dart';

import 'csg_journey_hub_sdk.dart';

/// Event to track user viewing a screen within the application.
/// if Named routing is used then it will give Screen name otherwise it will Screen name as Unknown
class CSGRouteObserver extends RouteObserver<PageRoute<dynamic>> {
  void _sendScreenView(PageRoute<dynamic> route) {
    var screenName = route.settings.name;
    if (screenName != null) {
      if (screenName == "/") {
        screenName = EventConstant.screenEventDefaultScreen;
      }
    } else {
      screenName = "Unknown";
    }
    CSGJourneyHubSDK.logEvent(
      name: EventConstant.screenEvent,
      requestParams: {'screen_name': screenName},
    );
  }

  @override
  void didPush(Route route, Route? previousRoute) {
    super.didPush(route, previousRoute);
    if (route is PageRoute) {
      _sendScreenView(route);
    }
  }

  @override
  void didReplace({Route? newRoute, Route? oldRoute}) {
    super.didReplace(newRoute: newRoute, oldRoute: oldRoute);
    if (newRoute is PageRoute) {
      _sendScreenView(newRoute);
    }
  }

  @override
  void didPop(Route route, Route? previousRoute) {
    super.didPop(route, previousRoute);
    if (previousRoute is PageRoute && route is PageRoute) {
      _sendScreenView(previousRoute);
    }
  }
}
