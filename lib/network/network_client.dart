import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

@immutable
class NetworkClient {
  static const NetworkClient _instance = NetworkClient._();

  const NetworkClient._();

  /// The NetworkClient singleton
  static NetworkClient get instance => _instance;

  /// Time out duration set to 60 sec
  static const int timeOutDuration = 60;
  static Map<String, String> header = {
    "Content-Type": "application/json",
    "Accept-Language": "en-US",
  };

  /// Calling POST to record an Event to Server
  Future<dynamic> recordEvent(String url, dynamic payloadObj) async {
    var uri = Uri.parse(url);
    var payload = json.encode(payloadObj);
    debugPrint("payLoad-----$payload");
    debugPrint("header $header");
    debugPrint(uri.toString());
    try {
      var response = await http
          .post(uri, headers: header, body: payload)
          .timeout(const Duration(seconds: timeOutDuration));
      debugPrint(utf8.decode(response.bodyBytes));
      return _processResponse(response);
    } catch (e) {
      throw Exception(
          "The request failed, This is most likely due to a networking error of some sort.");
    }
  }

  /// Processing response
  static dynamic _processResponse(http.Response response) {
    debugPrint("response code ${utf8.decode(response.bodyBytes)}");
    if (response.isOkResponse()) {
      debugPrint("API Success");
      return utf8.decode(response.bodyBytes);
    } else {
      throw Exception(
          "The request failed, This is most likely due to a networking error of some sort.");
    }
  }
}

extension on http.Response {
  bool isOkResponse() {
    return statusCode >= 200 && statusCode <= 299;
  }
}
