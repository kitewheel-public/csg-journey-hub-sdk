import 'package:flutter/material.dart';

@immutable
class Configuration {
  /// Endpoint of the collector, e.g. `http://localhost:9090`
  final String endpoint;
  final String key;

  const Configuration({required this.endpoint, required this.key});
}
