import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';

@immutable
class Reachability {
  /// The isInternetAvailable method is use to check is device is connected to network
  static Future<bool?> isInternetAvailable() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  /// The getConnectivitySource method is used to get source of connectivity
  static Future<String> getConnectivitySource() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult.name;
  }
}
