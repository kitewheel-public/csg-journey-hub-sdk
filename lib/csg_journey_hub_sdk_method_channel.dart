import 'package:csg_journey_hub_sdk/models/user_location.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'csg_journey_hub_sdk_platform_interface.dart';

/// An implementation of [CsgJourneyHubSdkPlatform] that uses method channels.
class MethodChannelCsgJourneyHubSdk extends CsgJourneyHubSdkPlatform {
  /// The method channel used to interact with the native platform.
  ///
  @visibleForTesting
  final methodChannel = const MethodChannel('csg_journey_hub_sdk');

  /// Used for unknown location
  static const String _unknown = "Unknown";

  /// The batteryLevel method is used to get battery level from native platform
  Future<double?> get batteryLevel async {
    var result = await methodChannel.invokeMethod('getBatteryLevel');
    return result;
  }

  /// The location method is used to get location from native platform
  Future<UserLocation?> get location async {
    var result = await methodChannel.invokeMethod('getLocation');
    if (result == null) {
      var location = const UserLocation(lat: _unknown, long: _unknown);
      return location;
    }
    var locationMap = result.cast<String, dynamic>();
    return UserLocation(
        lat: locationMap["lat"].toString(),
        long: locationMap["long"].toString());
  }

  /// The deviceDetails method is used for retrieving device details from native platform
  Future<Map<String, dynamic>> get deviceDetails async {
    var result = await methodChannel.invokeMethod('getDeviceDetails');
    return result?.cast<String, dynamic>();
  }
}
