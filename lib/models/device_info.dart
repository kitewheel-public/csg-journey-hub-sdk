import 'package:csg_journey_hub_sdk/models/base_event.dart';
import 'package:flutter/cupertino.dart';

/// DeviceInfo class is used to store device information
@immutable
class DeviceInfo implements Event {
  /// Device ipAddress
  final String? ipAddress;

  /// Operating System of device (ios/android)
  final String? operatingSystem;

  /// Channel (mobile)
  final String? channel;

  /// The current operating system version.
  final String? systemVersion;

  /// Carrier name
  final String? carrier;

  /// The name of network(wifi/mobile)
  final String? network;

  /// Device's default language
  final String? language;

  /// Device total storage size in GB
  final String? totalStorage;

  /// Device total memory size in GB
  final String? totalMemory;

  /// CSGJourneyHubSDK version
  final String? sdkVersion;

  /// Application version
  final String? appVersion;

  /// The manufacturer of the product/hardware.
  final String? manufacturer;

  /// The height of display in pixel
  final double? displayHeight;

  /// The width of display in pixel
  final double? displayWidth;

  /// Unique device installation id
  final String? installId;

  const DeviceInfo(
      {required this.ipAddress,
      required this.operatingSystem,
      required this.channel,
      required this.systemVersion,
      required this.carrier,
      required this.network,
      required this.language,
      required this.totalStorage,
      required this.totalMemory,
      required this.sdkVersion,
      required this.appVersion,
      required this.manufacturer,
      required this.displayHeight,
      required this.displayWidth,
      required this.installId});

  /// Converting DeviceInfo object to map
  @override
  Map<String, Object?> toMap() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["ipAddress"] = ipAddress;
    data["operatingSystem"] = operatingSystem;
    data["channel"] = channel;
    data["systemVersion"] = systemVersion;
    data["carrier"] = carrier;
    data["network"] = network;
    data["language"] = language;
    data["totalStorage"] = totalStorage;
    data["totalMemory"] = totalMemory;
    data["sdkVersion"] = sdkVersion;
    data["appVersion"] = appVersion;
    data["manufacturer"] = manufacturer;
    data["displayHeight"] = displayHeight;
    data["displayWidth"] = displayWidth;
    data["installId"] = installId;
    return data;
  }
}
