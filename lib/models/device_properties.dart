import 'package:csg_journey_hub_sdk/models/base_event.dart';
import 'package:flutter/cupertino.dart';

/// DeviceProperties class is used to store device properties
@immutable
class DeviceProperties implements Event {
  /// Device name.
  final String? name;

  /// The end-user-visible name for the end product.
  final String? model;

  /// The name of the current operating system.
  final String? systemName;

  /// The current operating system version.
  final String? osVersion;

  /// The Localized name of the device model.
  final String? localizedModel;

  /// ID value identifying the current device.
  final String? identifierForVendor;

  const DeviceProperties(
      {this.name,
      this.model,
      this.systemName,
      this.osVersion,
      this.localizedModel,
      this.identifierForVendor});

  /// Converting DeviceProperties object to map
  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["name"] = name;
    data["model"] = model;
    data["systemName"] = systemName;
    data["osVersion"] = osVersion;
    data["localizedModel"] = localizedModel;
    data["identifierForVendor"] = identifierForVendor;
    return data;
  }
}
