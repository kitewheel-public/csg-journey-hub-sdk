import 'package:csg_journey_hub_sdk/models/base_event.dart';
import 'package:flutter/cupertino.dart';

/// BatteryLevel model is used for storing initial battery leve and timestamp
/// battery level at the time of event
@immutable
class BatteryLevel implements Event {
  /// Battery level at the time of SDK initialize
  final double? initialSessionBatteryLevel;

  /// Battery level at the time of event
  final double? eventBatteryLevel;

  /// Timestamp of SDK initialize
  final int? initialSessionBatteryTime;

  const BatteryLevel(
      {this.initialSessionBatteryLevel,
      this.eventBatteryLevel,
      this.initialSessionBatteryTime});

  /// Converting BatteryLevel object to map
  @override
  Map<String, Object?> toMap() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['initialSessionBatteryLevel'] = initialSessionBatteryLevel;
    data['eventBatteryLevel'] = eventBatteryLevel;
    data['initialSessionBatteryTime'] = initialSessionBatteryTime;
    return data;
  }
}
