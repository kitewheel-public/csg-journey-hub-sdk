import 'package:csg_journey_hub_sdk/models/params.dart';
import 'package:flutter/cupertino.dart';

import 'base_event.dart';

/// EventData class is used to record event
@immutable
class EventData implements Event {
  /// Check the event is record automatically or manually
  final bool automatic;

  /// Set name of event
  final String name;

  /// Timestamp of event
  final int timestamp;

  /// Parameters
  final Params? params;

  const EventData(
      {required this.automatic,
      required this.name,
      required this.timestamp,
      this.params});

  /// Converting EventData object to map
  @override
  Map<String, dynamic> toMap() {
    return {
      "automatic": automatic,
      "name": name,
      "timestamp": timestamp,
      "params": params?.toMap(),
    };
  }
}
