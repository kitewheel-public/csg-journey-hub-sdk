import 'package:csg_journey_hub_sdk/models/base_event.dart';
import 'package:flutter/material.dart';

@immutable
class Params implements Event {
  /// Global parameters set at initialization
  final Map<String, dynamic>? globalParams;

  /// Request Parameters
  final Map<String, dynamic>? requestParams;

  const Params({this.globalParams, this.requestParams});

  @override
  Map<String, Object?> toMap() {
    return {
      "globalParams": globalParams,
      "requestParams": requestParams,
    };
  }
}
