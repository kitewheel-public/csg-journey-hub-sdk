import 'package:csg_journey_hub_sdk/models/base_event.dart';
import 'package:flutter/cupertino.dart';

/// UserLocation class is used for device location
@immutable
class UserLocation implements Event {
  /// Latitude
  final String? lat;

  /// Longitude
  final String? long;

  const UserLocation({this.lat, this.long});

  /// Converting UserLocation object to map
  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["lat"] = lat;
    data["long"] = long;
    return data;
  }
}
