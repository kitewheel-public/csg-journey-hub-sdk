import 'package:flutter/material.dart';

/// Abstract class for converting object to map
@immutable
abstract class Event {
  Map<String, Object?> toMap();
}
