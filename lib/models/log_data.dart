import 'package:flutter/cupertino.dart';

import 'base_event.dart';

/// LogData class is used to store device info, device properties,location, battery level and event information
@immutable
class LogData implements Event {
  /// Device information
  final Map<String, dynamic>? deviceInfo;

  /// Device properties
  final Map<String, dynamic>? deviceProperties;

  /// User location at time of event
  final Map<String, dynamic>? location;

  /// Device Battery Level
  final Map<String, dynamic>? batteryLevel;

  /// Event information
  final Map<String, dynamic>? eventInfo;

  const LogData(
      {this.deviceInfo,
      this.deviceProperties,
      this.location,
      this.batteryLevel,
      this.eventInfo});

  /// Converting LogData object to map
  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (deviceInfo != null) {
      data["deviceInfo"] = deviceInfo;
    }
    if (deviceProperties != null) {
      data["deviceProperties"] = deviceProperties;
    }
    if (location != null) {
      data["location"] = location;
    }
    if (batteryLevel != null) {
      data["batteryLevel"] = batteryLevel;
    }
    if (eventInfo != null) {
      data["eventInfo"] = eventInfo;
    }
    return data;
  }
}
