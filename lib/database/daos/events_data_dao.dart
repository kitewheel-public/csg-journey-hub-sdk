import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:sqflite_sqlcipher/sqflite.dart';

import '../../models/log_data.dart';
import '../database_helper.dart';

@immutable
class EventDataDAO {
  static const String tableName = "event_data";

  const EventDataDAO();

  /// Database object
  Future<Database> get _db async => await DatabaseHelper.shared.database;

  /// The insert method is used to save event information in local database
  Future insert(LogData logData) async {
    try {
      final db = await _db;
      var result = await db.rawInsert(
          "INSERT OR REPLACE INTO $tableName (timestamp,data) VALUES (?,?)", [
        logData.eventInfo?["timestamp"],
        json.encode(logData.toMap()).toString()
      ]);
      return result;
    } catch (error) {
      debugPrint(error.toString());
    }
  }

  /// The getEvents is used to get saved events from local database
  Future<Map<String, dynamic>?> getEvents() async {
    try {
      final db = await _db;
      var result = await db.rawQuery("SELECT * from $tableName LIMIT 1");
      if (result.isNotEmpty) {
        Map<String, dynamic> map = result[0];
        return jsonDecode(map["data"]);
      } else {
        return null;
      }
    } catch (error) {
      debugPrint(error.toString());
    }
    return null;
  }

  /// The delete method is use to delete synced record
  Future delete(int? timestamp) async {
    try {
      final db = await _db;
      await db
          .delete(tableName, where: "timestamp = ?", whereArgs: [timestamp]);
    } catch (error) {
      debugPrint(error.toString());
    }
  }
}
