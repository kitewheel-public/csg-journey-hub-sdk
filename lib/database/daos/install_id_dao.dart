import 'package:flutter/material.dart';
import 'package:sqflite_sqlcipher/sqflite.dart';

import '../database_helper.dart';

@immutable
class InstallIdDAO {
  static const String tableName = "install_id";

  const InstallIdDAO();

  /// Database object
  Future<Database> get _db async => await DatabaseHelper.shared.database;

  /// The insert method is used to save unique device id
  Future insert(String installId) async {
    try {
      final db = await _db;
      var result = await db.rawInsert(
          "INSERT OR REPLACE INTO $tableName (installId) VALUES (?)",
          [installId]);
      return result;
    } catch (error) {
      debugPrint(error.toString());
    }
  }

  /// The getInstallId method is used to get device unique id
  Future<String?> getInstallId() async {
    try {
      final db = await _db;
      var result = await db.rawQuery("SELECT * from $tableName");
      if (result.isNotEmpty) {
        return result[0]["installId"].toString();
      } else {
        return null;
      }
    } catch (error) {
      debugPrint(error.toString());
    }
    return null;
  }
}
