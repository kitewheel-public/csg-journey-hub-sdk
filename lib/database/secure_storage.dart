import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

@immutable
class SecureStorage {
  const SecureStorage();

  static const _storage = FlutterSecureStorage();

  static const _keyDBPassword = "db_password";

  /// The setDBPassword method is used to save database password in secure database
  static Future setDBPassword(String password) async {
    await _storage.write(key: _keyDBPassword, value: password);
  }

  /// The getDBPassword method is used to retrieve database password
  static Future<String?> getDBPassword() async {
    return await _storage.read(key: _keyDBPassword);
  }
}
