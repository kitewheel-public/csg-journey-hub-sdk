import 'package:flutter/material.dart';

@immutable
class DBSchema {
  const DBSchema();

  /// Event table schema
  static const eventTable =
      'CREATE TABLE event_data(timestamp varchar PRIMARY KEY, data TEXT)';

  /// Install id table schema
  static const installIdTable =
      'CREATE TABLE install_id(installId varchar PRIMARY KEY)';
}
