import 'package:flutter/material.dart';

@immutable
class EventConstant {
  const EventConstant();

  /// Automatic screen event tracking name
  static const screenEvent = "Screen Event";

  /// Default launcher screen name
  static const screenEventDefaultScreen = "Launcher Screen";
}
