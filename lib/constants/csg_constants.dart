import 'package:flutter/material.dart';

@immutable
class MobileOS {
  const MobileOS();

  /// iOS operatingSystem
  static const ios = "ios";

  /// Android operatingSystem
  static const android = "android";

  /// Channel mobile
  static const mobile = "mobile";
}
