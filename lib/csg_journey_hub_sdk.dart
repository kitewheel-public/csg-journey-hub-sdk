import 'dart:async';
import 'dart:io';

import 'package:csg_journey_hub_sdk/constants/csg_event_constants.dart';
import 'package:csg_journey_hub_sdk/database/daos/events_data_dao.dart';
import 'package:csg_journey_hub_sdk/models/device_properties.dart';
import 'package:csg_journey_hub_sdk/models/event_data.dart';
import 'package:csg_journey_hub_sdk/models/log_data.dart';
import 'package:csg_journey_hub_sdk/models/params.dart';
import 'package:csg_journey_hub_sdk/network/configuration.dart';
import 'package:csg_journey_hub_sdk/network/network_client.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:uuid/uuid.dart';

import 'constants/csg_constants.dart';
import 'csg_journey_hub_sdk_method_channel.dart';
import 'csg_route_observer.dart';
import 'database/daos/install_id_dao.dart';
import 'database/database_helper.dart';
import 'ip_address/ip_address.dart';
import 'models/battery_level.dart';
import 'models/device_info.dart';
import 'network/reachability.dart';

class CSGJourneyHubSDK {
  /// End point configuration
  static late Configuration _configuration;

  /// SDK version
  static const _csgJourneySDKVersion = "0.0.1";

  /// Check event tracking is ON/OFF
  static bool _isEventTrackingEnabled = true;

  ///Battery level at time of sdk initialize
  static double? _initialBatteryLevel = -1;

  /// Timestamp of battery level at time of sdk initialize
  static int? _initialBatteryLevelTimeStamp = -1;

  /// The method channel used to interact with the native platform.
  static final _methodChannel = MethodChannelCsgJourneyHubSdk();

  /// Unique device id
  static String? _installId;

  /// Check sync is in progress
  static bool _isSyncInProgress = false;

  /// Periodic timer to sync saved events
  static Timer? _periodicTimer;

  /// Periodic time to sync saved event. default value is 1t seconds
  static const int _periodicTime = 15;

  /// Global parameters while initialization
  static Map<String, dynamic> _globalParams = <String, dynamic>{};

  const CSGJourneyHubSDK();

  /// The initializeApp used to configure CSGJourneySdk
  static Future<void> initializeApp(
      {required String endpoint,
      required String key,
      Map<String, dynamic> globalParams = const {}}) async {
    _configuration = Configuration(endpoint: endpoint, key: key);
    _globalParams = globalParams;
    _initialBatteryLevel = await _methodChannel.batteryLevel;
    _initialBatteryLevelTimeStamp =
        DateTime.now().toUtc().millisecondsSinceEpoch;
    await DatabaseHelper.shared.initializeDB();
    _installId = await _getInstallId();

    await setPeriodicTimer();
  }

  /// Set periodic timer to sync events
  static Future<void> setPeriodicTimer() async {
    if (await Reachability.isInternetAvailable() == true) {
      _periodicTimer = Timer.periodic(const Duration(seconds: _periodicTime),
          (Timer timer) async {
        if (await Reachability.isInternetAvailable() == true) {
          if (!_isSyncInProgress) {
            await _syncEvent();
          }
        }
      });
    } else {
      if (_periodicTimer != null) {
        if (_periodicTimer!.isActive) {
          _periodicTimer?.cancel();
        }
      }
    }
  }

  /// ON/OFF event tracking
  static setEventTrackingEnabled(bool value) {
    _isEventTrackingEnabled = value;
  }

  /// Get device information from native platform
  static Future<DeviceInfo> _getDeviceInfo() async {
    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    final operatingSystem = Platform.isIOS ? MobileOS.ios : MobileOS.android;
    var deviceDetails = await _methodChannel.deviceDetails;
    var carrier = deviceDetails["carrier"].toString().isEmpty
        ? "Unknown"
        : deviceDetails["carrier"];

    String? systemVersion = "";
    String? manufacturer = "";
    if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfoPlugin.iosInfo;
      systemVersion = iosInfo.systemVersion;
      manufacturer = "Apple";
    } else if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfoPlugin.androidInfo;
      systemVersion = androidInfo.version.release;
      manufacturer = androidInfo.manufacturer;
    }

    return DeviceInfo(
        ipAddress: "",
        network: "",
        operatingSystem: operatingSystem,
        channel: MobileOS.mobile,
        systemVersion: systemVersion,
        carrier: carrier,
        language: Platform.localeName,
        totalStorage: "${deviceDetails["totalStorage"]} GB",
        totalMemory: "${deviceDetails["totalMemory"]} GB",
        sdkVersion: _csgJourneySDKVersion,
        appVersion: deviceDetails["version"],
        manufacturer: manufacturer,
        displayHeight: deviceDetails["screenHeight"],
        displayWidth: deviceDetails["screenWidth"],
        installId: _installId);
  }

  /// Get device properties from native platform
  static Future<DeviceProperties> _getDeviceProperties() async {
    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfoPlugin.iosInfo;
      return DeviceProperties(
        name: iosInfo.name,
        model: iosInfo.utsname.machine,
        systemName: iosInfo.systemName,
        osVersion: iosInfo.systemVersion,
        localizedModel: iosInfo.localizedModel,
        identifierForVendor: iosInfo.identifierForVendor,
      );
    } else {
      AndroidDeviceInfo androidInfo = await deviceInfoPlugin.androidInfo;
      return DeviceProperties(
        name: androidInfo.device,
        model: androidInfo.model,
        systemName: "Android",
        osVersion: androidInfo.version.sdkInt.toString(),
        localizedModel: androidInfo.device,
        identifierForVendor: androidInfo.id,
      );
    }
  }

  /// Log an event by SDK user
  static logEvent(
      {required String name, Map<String, Object>? requestParams}) async {
    debugPrint('$name - $requestParams');
    final dbInitialized = await DatabaseHelper.shared.isDBInitialized();
    if (!dbInitialized) {
      Future.delayed(const Duration(seconds: 1), () {
        logEvent(name: name, requestParams: requestParams);
      });
      return;
    }
    if (_isEventTrackingEnabled) {
      var deviceProperties = await _getDeviceProperties();
      var deviceInfo = await _getDeviceInfo();
      _installId ??= await _getInstallId();

      var batteryPercentage = await _methodChannel.batteryLevel;

      BatteryLevel batteryLevel = BatteryLevel(
          initialSessionBatteryLevel: _initialBatteryLevel,
          eventBatteryLevel: batteryPercentage,
          initialSessionBatteryTime: _initialBatteryLevelTimeStamp);

      Params params =
          Params(requestParams: requestParams, globalParams: _globalParams);
      final event = EventData(
          automatic: name == EventConstant.screenEvent,
          name: name,
          timestamp: DateTime.now().toUtc().millisecondsSinceEpoch,
          params: params);
      var location = await _methodChannel.location;
      var logData = LogData(
          deviceInfo: deviceInfo.toMap(),
          deviceProperties: deviceProperties.toMap(),
          location: location?.toMap(),
          batteryLevel: batteryLevel.toMap(),
          eventInfo: event.toMap());

      logData.deviceInfo?["installId"] = _installId;

      if (await Reachability.isInternetAvailable() == true) {
        logData.deviceInfo?["network"] =
            await Reachability.getConnectivitySource();

        final ip = await IPAddress.getIPAddress();
        logData.deviceInfo?["ipAddress"] = ip.toString();
      } else {
        logData.deviceInfo?["network"] = "";
        logData.deviceInfo?["ipAddress"] = "";
      }

      debugPrint(logData.toMap().toString());
      _saveEventData(logData);
    }
  }

  /// Save log event in local database
  static _saveEventData(LogData logData) async {
    await const EventDataDAO().insert(logData);
  }

  /// Get logged event from local database
  static Future<Map<String, dynamic>?> _getLoggedData() async {
    return await const EventDataDAO().getEvents();
  }

  /// Get unique device id
  static Future<String?> _getInstallId() async {
    var installId = await const InstallIdDAO().getInstallId();
    if (installId == null || installId.isEmpty) {
      var uuid = const Uuid();
      installId = uuid.v4();
      await const InstallIdDAO().insert(installId);
    }
    return installId;
  }

  /// Recording events to sever
  static Future _recordEventData(Map<String, dynamic> logData) async {
    if (_configuration.key.isEmpty || _configuration.key == "API_KEY") {
      return;
    }
    if (await Reachability.isInternetAvailable() == true) {
      if (logData["deviceInfo"]["network"] == null ||
          logData["deviceInfo"]["network"].toString().isEmpty) {
        logData["deviceInfo"]["network"] =
            await Reachability.getConnectivitySource();
      }

      if (logData["deviceInfo"]["ipAddress"] == null ||
          logData["deviceInfo"]["ipAddress"].toString().isEmpty) {
        logData["deviceInfo"]["ipAddress"] = await IPAddress.getIPAddress();
      }

      final url = _configuration.endpoint + _configuration.key;
      await NetworkClient.instance
          .recordEvent(url, logData)
          .then((value) async {
        await const EventDataDAO().delete(logData["eventInfo"]["timestamp"]);
        debugPrint(value.toString());
      }).onError((error, stackTrace) {
        debugPrint(error.toString());
      });
    }
  }

  /// Syncing saved events to the server
  static Future<void> _syncEvent() async {
    Map<String, dynamic>? record = await _getLoggedData();
    if (record != null) {
      _isSyncInProgress = true;
      await _recordEventData(record).then((value) async {
        await _syncEvent();
      });
    } else {
      _isSyncInProgress = false;
    }
  }

  /// Get Route observer for automatic screen tracking
  static getCSGRouteObserver() {
    return CSGRouteObserver();
  }
}
