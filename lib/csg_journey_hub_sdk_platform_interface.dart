import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'csg_journey_hub_sdk_method_channel.dart';

abstract class CsgJourneyHubSdkPlatform extends PlatformInterface {
  /// Constructs a CsgJourneyHubSdkPlatform.
  CsgJourneyHubSdkPlatform() : super(token: _token);

  static final Object _token = Object();

  static CsgJourneyHubSdkPlatform _instance = MethodChannelCsgJourneyHubSdk();

  /// The default instance of [CsgJourneyHubSdkPlatform] to use.
  ///
  /// Defaults to [MethodChannelCsgJourneyHubSdk].
  static CsgJourneyHubSdkPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [CsgJourneyHubSdkPlatform] when
  /// they register themselves.
  static set instance(CsgJourneyHubSdkPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }
}
