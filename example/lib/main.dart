import 'package:csg_journey_hub_sdk/csg_journey_hub_sdk.dart';
import 'package:flutter/material.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  CSGJourneyHubSDK.initializeApp(
      endpoint: "API_END_POINT",
      key: "API_KEY",
      globalParams: {"appName": "CSGJourneyHubSDK Example"});
  CSGJourneyHubSDK.setEventTrackingEnabled(true);

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorObservers: [CSGJourneyHubSDK.getCSGRouteObserver()],
      home: const HomeScreen(),
      routes: {
        'screen1': (context) => const Screen1(),
        'screen2': (context) => const Screen2(),
      },
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool _optInEventTracking = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('CSGJourneyHub Plugin Example'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 20.0),
              ElevatedButton(
                child: const Text('Go to Screen 1'),
                onPressed: () => Navigator.of(context).pushNamed('screen1'),
              ),
              const SizedBox(height: 20.0),
              ElevatedButton(
                child: const Text('Go to Screen 2'),
                onPressed: () => Navigator.of(context).pushNamed('screen2'),
              ),
              const SizedBox(height: 20.0),
              TextButton(
                  onPressed: () {
                    CSGJourneyHubSDK.logEvent(
                        name: "Home Button Clicked",
                        requestParams: {
                          'screen_name': "Home Screen",
                        });
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text("Home Event recorded.")));
                  },
                  child: const Text('Home Button Click')),
              const SizedBox(height: 20.0),
              const Text("Opt In Event tracking ON and OFF"),
              Switch(
                  value: _optInEventTracking,
                  onChanged: (value) {
                    setState(() {
                      _optInEventTracking = value;
                      CSGJourneyHubSDK.setEventTrackingEnabled(
                          _optInEventTracking);
                    });
                  })
            ],
          ),
        ),
      ),
    );
  }
}

class Screen1 extends StatefulWidget {
  const Screen1({Key? key}) : super(key: key);

  @override
  State<Screen1> createState() => _Screen1State();
}

class _Screen1State extends State<Screen1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Screen 1"),
        ),
        body: Center(
            child: ElevatedButton(
                onPressed: () {
                  CSGJourneyHubSDK.logEvent(
                      name: "Login Button Click",
                      requestParams: {
                        'login_event': "Login Button Clicked",
                      });
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text("Login Button Click event recorded.")));
                },
                child: const Text("Login Button Click"))));
  }
}

class Screen2 extends StatelessWidget {
  const Screen2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Screen 2"),
        ),
        body: Center(
            child: ElevatedButton(
                onPressed: () {
                  CSGJourneyHubSDK.logEvent(
                      name: "Signup Button Click",
                      requestParams: {
                        'signup': "Signup Button Clicked",
                        "user_name": "Jack"
                      });
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text("Signup Button Clicked recorded.")));
                },
                child: const Text("Signup Button Click"))));
  }
}
